/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;

class PolyphaseBuffer<T extends Serializable>
{
	private Comparator<T> comparer;
	private StreamValueComparator<T> pairComparer;
	
	private PriorityQueue<StreamValuePair<T>> currentRun;
	private PriorityQueue<StreamValuePair<T>> nextRun;
	private List<PolyphaseStream<T>> empties;
	private PolyphaseStream<T> output;
	private int currentOutput = 0;
	private T lastWrite;

	public PolyphaseBuffer(List<PolyphaseStream<T>> streams, Comparator<T> comparer) throws IOException
	{
		this.comparer = comparer;
		pairComparer = new StreamValueComparator<T>(comparer);
		currentRun = new PriorityQueue<StreamValuePair<T>>(streams.size() - 1, pairComparer);
		nextRun = new PriorityQueue<StreamValuePair<T>>(streams.size() - 1, pairComparer);
		empties = new ArrayList<PolyphaseStream<T>>();
		
		for (PolyphaseStream<T> stream : streams)
		{
			stream.openRead();
			if (stream.hasNext())
			{
				stream.openRead();
				T value = stream.read();
				StreamValuePair<T> input = new StreamValuePair<T>(stream, value);
				currentRun.add(input);
			}
			else
			{
				empties.add(stream);
			}
		}
		
		output = empties.remove(0);
		output.openWrite();
	}

	public T read() throws IOException
	{
		if (currentRun.size() == 0)
			swapRuns();
		
		StreamValuePair<T> input = currentRun.poll();
		PolyphaseStream<T> stream = input.buffer;
		T value = input.getValue();
		
		if (stream.hasNext())
		{
			T next = (T) stream.read();
			input.setValue(next);
			
			if (comparer.compare(next, value) >= 0)
				currentRun.add(input);
			else
			{
				nextRun.add(input);
				if (currentRun.size() == 0)
				{
					swapRuns();
					swapOutput();
				}
			}
		}
		else
		{
			System.err.println("Buffer file finished - active files: " + currentRun.size() + ", remaining: " + nextRun.size() + ", empties: " + empties.size());
			empties.add(stream);
		}
		
		return value;
	}
	
	public void write(T value) throws IOException
	{
		output.write(value);
		System.err.println("Writing " + value);
	}
	
	public void swapRuns()
	{
		System.err.println("Merging next run - remaining files: " + nextRun.size());
		PriorityQueue<StreamValuePair<T>> swap = currentRun;
		currentRun = nextRun;
		nextRun = swap;
	}
	
	public void swapOutput() throws IOException
	{
		if (empties.size() > 0)
		{
			System.err.println("Swapping output file");
			
			output.openRead();
			T next = (T) output.read();
			StreamValuePair<T> input = new StreamValuePair<T>(output, next);
			nextRun.add(input);
			
			output = empties.remove(0);
			output.openWrite();
		}
		
		if (empties.size() > 0)
		{
			System.err.println("Dropping " + empties.size() + " empty files");
			empties.clear();
		}
	}
	
	public boolean hasNext()
	{
		return (currentRun.size() > 0 || nextRun.size() > 0);
	}
}
