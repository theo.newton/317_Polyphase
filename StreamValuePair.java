/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.Serializable;
import java.util.Comparator;

class StreamValuePair<T extends Serializable>
{
	public final PolyphaseStream<T> buffer;
	private T value;
	private int run;

	public StreamValuePair(PolyphaseStream<T> buffer, T value)
	{
		this.buffer = buffer;
		this.value = value;
	}

	public T getValue() { return value; }

	public void setValue(T value) { this.value = value; }
}
