/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge

	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.util.*;
import java.lang.*;

class HeapArray<T>{

	private T[] heapArray;
	private int heapSize, currentIndex, parentIndex, capacity, bufferSize, position = 0;
	private Comparator<T> comparator;

	// Constructor
	public HeapArray(T[] memory, Comparator<T> comparator){
		this.comparator = comparator;
		this.heapArray = memory;
		this.capacity = memory.length-1;
	}

	// Adds the given element to the heap
	public void heapAdd(T value){
		if(capacity <= heapSize + bufferSize)
			System.out.println("At max capacity, cannot add more");
		heapSize++;
		heapArray[heapSize] = value;
		upHeap(heapSize);
		capacity--;
		
	}	

	// Returns the first element on the heap, without removing it
	public T peek(){
		if(heapArray.length == 0)
			System.out.println("Cannot peek, heapArray is empty");
		
		T value = heapArray[1];
		return value;
	}


	// Adds the given element to an array
	public void addToArray(T value){
		if(capacity <= heapSize + bufferSize)
			throw new IndexOutOfBoundsException();
		if(capacity > 0){
			heapArray[capacity-bufferSize] = value;
			System.out.println(value + " awfaw");
			printArray();
			bufferSize ++;
			//sort();
		}		
	}	

	// public void sort(){
	// 	for(int i = 0; i < bufferSize; i++){			
	// 		T value = heapArray[capacity-bufferSize + i];
	// 		if(comparator.compare(heapArray[index], heapArray[index*2]) > 0 || index * 2 + 1 <= heapSize && comparator.compare(heapArray[index], heapArray[index * 2 + 1]) > 0){
	// 			T swap = heapArray[index];
 //    	 		heapArray[index] =  heapArray[min];
 //    	 		heapArray[min] =  swap;
	// 		}			
	// 	}
	// }


	// Removes and returns the first element on the heap
	public T poll(){
		T value = heapArray[1];
		printArray();
		heapArray[1] = heapArray[heapSize--];																																																																																																																																																																																														
		// We need to fix the heap since something got removed
		downHeap(1);
		return value;
	}

	public void printArray(){
		T value;
		for(int x = 0; x < heapSize; x ++){
			value = heapArray[x];
		System.out.println(value);
		}

		for(int y = bufferSize; y > 0 ; y--){
			value = heapArray[capacity - y];
		//System.out.println(value);
		}

	}


	// Organise the heap recursivley
    public void downHeap(int index){
    	// Check for children
    	if(index * 2 > heapSize)
    		return;
    	// Check the parent against the left child, check the right child exists, check if the parent is bigger than the right child
    	if(comparator.compare(heapArray[index], heapArray[index*2]) > 0 || index * 2 + 1 <= heapSize && comparator.compare(heapArray[index], heapArray[index * 2 + 1]) > 0){
    		int min = index * 2; // This must exist, it is the left child.
    		// Check if right child exists and is less than left child
    		if(index * 2 + 1 <= heapSize && comparator.compare(heapArray[index*2],heapArray[index * 2 + 1]) > 0 ){
    			min++;
    		}
    	// Swap the smallest of the parent and children with the parent
    	T swap = heapArray[index];
    	 heapArray[index] =  heapArray[min];
    	 heapArray[min] =  swap;
    	 downHeap(min); // recursively downheap
    	}
    }

		// Inserts the item into the heap
	public void insert(T value){
		// Put the value into the heapArray and then increase it's number
		heapArray[++currentIndex] = value;
		upHeap(currentIndex);
	}

	// Organises the heap after an item is added to the heap
	public void upHeap(int index){
		if (index < 1 || index > heapSize) {} 
		T value = heapArray[index];
		while (index > 1) {
			int parentIndex = index / 2;
			if (comparator.compare(value,heapArray[parentIndex]) >= 0) break;
			heapArray[index] = heapArray[parentIndex];
			index = parentIndex;
		}
		heapArray[index] = value;
	}

	// Returns the number of elements in the heap
	public int heapSize(){
		return heapSize;
	}
}
