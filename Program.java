import java.util.*;
import java.io.*;

class Program
{
	public static void main(String[] args) throws Exception
	{
		Scanner input = new Scanner(new FileInputStream(args[0]));
		PrintWriter output = new PrintWriter(new FileOutputStream(args[1]));
		System.err.println("Creating sorter");
		PolyphaseSorter sorter = new PolyphaseSorter(input, output);
		System.err.println("Sorting");
		sorter.sort();
	}
}
