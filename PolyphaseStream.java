/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Comparator;

class PolyphaseStream<T extends Serializable>
{
	enum Mode {
		Read,
		Write,
		Closed
	}
	
	public final File file;
	public final Comparator<T> comparer;
	private Mode mode = Mode.Closed;
	private ObjectInput input = null;
	private ObjectOutput output = null;
	private T last = null;
	private T next = null;

	public PolyphaseStream(File file, Comparator<T> comparer)
	{
		this.file = file;
		this.comparer = comparer;
	}

	@SuppressWarnings("unchecked")
	public T read() throws IOException
	{
		if (hasNext())
		{
			T result = next;
			try
			{
				Object raw = input.readObject();
				next = (T) raw;
				return result;
			}
			catch (ClassCastException e)
			{
				throw new IOException();
			}
			catch (ClassNotFoundException e)
			{
				throw new IOException();
			}
		}
		else
			throw new IOException();
	}
	
	public void write(T value) throws IOException
	{
		if (mode == Mode.Write)
		{
			output.writeObject(value);
			last = value;
		}
		else
		{
			System.err.println(mode);
			throw new IOException();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void openRead() throws IOException
	{
		close();
		FileInputStream stream = new FileInputStream(file);
		input = new ObjectInputStream(stream);
		try
		{
			Object raw = input.readObject();
			next = (T) raw;
		}
		catch (ClassCastException e)
		{
			throw new IOException();
		}
		catch (ClassNotFoundException e)
		{
			throw new IOException();
		}
		mode = Mode.Read;
	}
	
	public void openWrite() throws IOException
	{
		close();
		FileOutputStream stream = new FileOutputStream(file);
		output = new ObjectOutputStream(stream);
		mode = Mode.Write;
	}
	
	public void close() throws IOException
	{
		if (mode == Mode.Read)
		{
			input.close();
		}
		
		if (mode == Mode.Write)
		{
			output.writeObject(null);
			output.close();
		}
		
		mode = Mode.Closed;
	}
	
	public boolean hasNext()
	{
		if (mode != Mode.Read)
			return false;
		
		return next != null;
	}
	
	public void printContents() throws IOException
	{
		close();
		openRead();
		System.err.print("Contents:");
		while(hasNext())
			System.err.print(" " + read());
		close();
		System.err.println();
	}
}
