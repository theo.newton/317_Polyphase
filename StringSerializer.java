/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

class StringSerializer implements Serializer<String>
{
	private Scanner input;
	private PrintWriter output;
	private File file;
	
	public StringSerializer(File file) throws IOException
	{
		this.file = file;
		restart();
	}
	
	// Reads and returns one instance of type T from the input stream
	public String read() throws IOException
	{
		String result = input.nextLine();
		System.err.println("Read " + result + " from " + file);
		return result;
	}
	
	// Writes the given instance of type T to the output stream
	public void write(String value) throws IOException
	{
		output.println(value);
	}
	
	// Resets the position of both the read and the write streams to the beginning
	public void restart() throws IOException
	{
		close();
		input = new Scanner(new FileInputStream(file));
		output = new PrintWriter(file);
	}
	
	// Returns whether the read stream has more values to be read
	public boolean hasNext()
	{
		boolean result = input.hasNextLine();
		if (!result)
			System.err.println("Reached end of " + file);
		return result;
	}

	// Closes and releases any underlying resources
	public void close() throws IOException
	{
		if (input != null)
			input.close();
		if (output != null)
			output.close();
	}
}
