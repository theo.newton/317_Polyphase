/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;

class PolyphaseSorter
{
	private Comparator<String> comparer;
	private PolyphaseBuffer<String> buffer;

	private PriorityQueue<String> currentRun;
	private PriorityQueue<String> nextRun;

	private Scanner input;
	private PrintWriter output;
	
	private int runs = 1;
	private int passes = 0;
	private int runSize = 7;
	private int numberOfFiles = 7;
	private File tempDirectory = new File(".");

	public PolyphaseSorter(Scanner input, PrintWriter output) throws IOException
	{
		this.comparer = new StringComparator();
		currentRun = new PriorityQueue<String>(runSize, comparer);
		nextRun = new PriorityQueue<String>(runSize, comparer);
		this.input = input;
		this.output = output;
		splitRun();
	}

	private void splitRun() throws IOException
	{
		File file1 = File.createTempFile("file1", ".tmp", tempDirectory);
		File file2 = File.createTempFile("file2", ".tmp", tempDirectory);
		File file3 = File.createTempFile("file3", ".tmp", tempDirectory);
		PolyphaseStream<String> buffer1 = new PolyphaseStream<String>(file1, comparer);
		PolyphaseStream<String> buffer2 = new PolyphaseStream<String>(file2, comparer);
		PolyphaseStream<String> buffer3 = new PolyphaseStream<String>(file3, comparer);
		buffer1.openWrite();
		buffer2.openWrite();
		buffer3.openWrite();
		
		int i = 0;
		while (input.hasNextLine())
		{
			String run = input.nextLine();
			System.err.println(i + ", " + run);
			if (i % 2 == 0)
			{
				System.err.println("Writing to file 1");
				buffer1.write(run);
			}
			else
			{
				System.err.println("Writing to file 2");
				buffer2.write(run);
			}
			i++;
		}
		input.close();
		
		List<PolyphaseStream<String>> streams = new ArrayList<PolyphaseStream<String>>();
		streams.add(buffer1);
		streams.add(buffer2);
		streams.add(buffer3);
		
		buffer = new PolyphaseBuffer<String>(streams, comparer);
	}

	public void sort() throws IOException
	{
		System.err.println("Sorting...");
		while(buffer.hasNext() && currentRun.size() < runSize)
		{
			String next = buffer.read();
			System.err.println("Adding " + next + " to current run");
			currentRun.add(next);
			//printBuffers();
		}
		
		while(buffer.hasNext())
		{
			String next = buffer.read();
			String min = currentRun.peek();
			
			if (comparer.compare(next, min) >= 0)
			{
				addToCurrentRun(next);
			}
			else
			{
				addToNextRun(next);
			}
		}
	}
	
	public void printBuffers()
	{
		System.err.print("Current: ");
		for (String value : currentRun)
				System.err.print(value + ", ");
		System.out.println();
		System.err.print("   Next: ");
		for (String value : nextRun)
			System.err.print(value + ", ");
		System.out.println();
	}
	
	private void addToNextRun(String value) throws IOException
	{
		//System.out.println("Adding " + value + " to next run");
		String min = currentRun.poll();
		buffer.write(min);
		nextRun.add(value);
		//printBuffers();
		
		if (currentRun.size() == 0)
		{
			//System.err.println("Finishing current run");
			PriorityQueue<String> swap = currentRun;
			currentRun = nextRun;
			nextRun = swap;
			runs++;
		}
	}
	
	private void addToCurrentRun(String value) throws IOException
	{
		//System.out.println("Adding " + value + " to current run");
		String next = currentRun.poll();
		currentRun.add(value);
		buffer.write(next);
		//printBuffers();
	}
}
