/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.IOException;

interface Serializer<T>
{
	// Reads and returns one instance of type T from the input stream
	public T read() throws IOException;
	
	// Writes the given instance of type T to the output stream
	public void write(T value) throws IOException;
	
	// Resets the position of both the read and the write streams to the beginning
	public void restart() throws IOException;
	
	// Returns whether the read stream has more values to be read
	public boolean hasNext();
	
	// Closes and releases any underlying resources
	public void close() throws IOException;
}
