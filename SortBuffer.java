/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.util.Comparator;
import java.util.PriorityQueue;

class SortBuffer<T>
{
	private Comparator<T> comparer;

	private PriorityQueue<T> currentRun;
	private PriorityQueue<T> nextRun;
	
	public SortBuffer(Comparator<T> comparer)
	{
		this.comparer = comparer;
		currentRun = new PriorityQueue<T>(7, comparer);
		nextRun = new PriorityQueue<T>(7, comparer);
	}
	
	public int size()
	{
		return currentRun.size() + nextRun.size();
	}
	
	public void fill(T[] values)
	{
		currentRun.clear();
		nextRun.clear();
		for(T value : values)
			currentRun.add(value);
	}
	
	public T poll()
	{
		T min = currentRun.poll();
		if (currentRun.size() == 0 && nextRun.size() > 0)
			swapRuns();
		return min;
	}
	
	public T push(T value)
	{
		T min = currentRun.poll();
		if (comparer.compare(value, min) >= 0)
		{
			currentRun.add(value);
		}
		else
		{
			nextRun.add(value);
			if (currentRun.size() == 0)
				swapRuns();
		}
		return min;
	}
	
	private void swapRuns()
	{
		System.err.println("Swapping runs");
		PriorityQueue<T> swap = currentRun;
		currentRun = nextRun;
		nextRun = swap;
	}
}
