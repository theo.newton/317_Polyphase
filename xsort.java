/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge

	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.Object;
import java.lang.String;
import java.util.Arrays;
import java.util.Scanner;


public class xsort{

	public static void main(String[] args){
		// Initialise the defaults if commands are ommitted
			int runsize = 7, numFiles = 7;
			String tempdir = System.getProperty("user.dir");

			String inputfilename = null, outputfilename = null;
		try{			
			String[] array;

			for (int i = 0; i < args.length; i++) {
				/* 
				* 	-r runsize — where "runsize" is a positive integer not less than 7 that specifies the minimum run length (i.e. heap size) 
				*  	to be used for creating the initial runs, and the runs are to be created using the replacement selection strategy. 
				*	If this argument is omitted by the user then the default should be 7. 
				*/
				if(args[i].equals("-r")){
					 runsize = Integer.parseInt(args[++i]);
					 continue;
				}
				/* 
				*	 -k numfiles — where "numfiles" is the total number of temporary files to be used for input and output, with a default value of 7 if this argument is omitted by the user. 
				*/
				if(args[i].equals("-k")){
					numFiles = Integer.parseInt(args[++i]);
					continue;
				}
				/*
				*	 -o outputfilename — where outputfilename is the name of the file where the final sorted data is to be stored. If omitted then final output is to be standard output. 
				*/
				if(args[i].equals("-o")){
					outputfilename = args[++i];
					continue;
				}
				if(!(args[i].equals("-o"))){
					 outputfilename = args[++i];
					System.out.println();
					continue;
				}
				/*
				*	 -d tempdir — where "tempdir" is the name of the directory where temporary files are created. If this argument is omitted, the default should be the current directory. 
				*/
				if(args[i].equals("-d")){
					tempdir = args[++i];
					continue;
				}
				if(!(args[i].equals("-d"))){
					tempdir =args[++i];			
					continue;
				}
				/*
				*	 inputfilename — which is the name of the file containing the data to be sorted. If omitted, then data is obtained from standard input. 
				*/
				if(inputfilename == null){
					inputfilename = args[++i];
				}
				else throw new Exception();
			}
			
    			System.out.println("You want a runsize of " + runsize);
    			System.out.println("You want the number of temp files to be " + numFiles);
    			System.out.println("You want the output file name to be " + outputfilename);
    			System.out.println("Your input file name is " + inputfilename);
    			System.out.println("Where you want temporary files to be made: " + tempdir);
    	}
    
    	catch(Exception e){
    		System.out.println("Oops! An Error has occurred!");
    		System.out.printf("\n -r [number of runs (default: 7)] \n -k [number of temporary files (default: 7)] \n -o [name of your output file (default: standard output) \n -d [the directory for temporary files (default:" + System.getProperty("user.dir") + ") \n name of input file (default: Standard input) \n");

    	}
    }
}
