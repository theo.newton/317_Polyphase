/*
	COMP-317-15A Assignment 1 - Polyphase sort-merge
	
	Theo Newton 117 5457
	Andrew Williamson 121 5027
*/

import java.io.Serializable;
import java.util.Comparator;

class StreamValueComparator<T extends Serializable> implements Comparator<StreamValuePair<T>>
{
	public final Comparator<T> comparer;

	public StreamValueComparator(Comparator<T> comparer)
	{
		this.comparer = comparer;
	}

	public int compare(StreamValuePair<T> pair1, StreamValuePair<T> pair2)
	{
		return comparer.compare(pair1.getValue(), pair2.getValue());
	}

	public boolean equals(Object obj)
	{
		try
		{
			@SuppressWarnings("unchecked")
			StreamValueComparator<T> other = (StreamValueComparator<T>) obj;
			return comparer.equals(other.comparer);
		}
		catch(ClassCastException e)
		{
			return false;
		}
	}
}
